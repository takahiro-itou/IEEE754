
#if 0
CXX=g++
program=MakeHalfToDouble
${CXX}  -o bin_${program}.exe  ${program}.cpp;
exit  0
#endif

#include    <iomanip>
#include    <iostream>
#include    <stdint.h>

int main(int argc, char * argv[])
{
    int         i;
    uint64_t    val;

    //  正のゼロ。          //
    //  0x0000              //
    std::cout   <<  std::hex    <<  std::setw(16);
    std::cout   <<  "0x0000000000000000,\n";

    //  正の非正規化数。    //
    //  0x0001              //
    std::cout   <<  "0x3e70000000000000,\n";

    //  0x0002 ... 0x0003   //
    std::cout   <<  "0x3e80000000000000,\n";
    std::cout   <<  "0x3e88000000000000,\n";

    //  0x0004 ... 0x0007   //
    for ( int i = 4; i <= 7; ++ i ) {
        val = 0x3e80000000000000 + (static_cast<uint64_t>(i) << 50);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x0008 ... 0x000f   //
    for ( int i = 8; i <= 15; ++ i ) {
        val = 0x3e90000000000000 + (static_cast<uint64_t>(i) << 49);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x0010 ... 0x001f   //
    for ( int i = 16; i <= 31; ++ i ) {
        val = 0x3ea0000000000000 + (static_cast<uint64_t>(i) << 48);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x0020 ... 0x003f   //
    for ( int i = 32; i <= 63; ++ i ) {
        val = 0x3eb0000000000000 + (static_cast<uint64_t>(i) << 47);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x0040 ... 0x007f   //
    for ( int i = 64; i <= 127; ++ i ) {
        val = 0x3ec0000000000000 + (static_cast<uint64_t>(i) << 46);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x0080 ... 0x00ff   //
    for ( int i = 128; i <= 255; ++ i ) {
        val = 0x3ed0000000000000 + (static_cast<uint64_t>(i) << 45);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x0100 ... 0x01ff   //
    for ( int i = 256; i <= 511; ++ i ) {
        val = 0x3ee0000000000000 + (static_cast<uint64_t>(i) << 44);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x0200 ... 0x03ff   //
    for ( int i = 512; i <= 1023; ++ i ) {
        val = 0x3ef0000000000000 + (static_cast<uint64_t>(i) << 43);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  正の正規化数。      //
    //  0x0400 ... 0x7bff   //
    for ( int i = 0; i <= 30719; ++ i ) {
        val = 0x3f10000000000000 + (static_cast<uint64_t>(i) << 42);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  INF, NaN            //
    //  0x7c00 ... 0x7fff   //
    for ( int i = 0; i <= 1023; ++ i ) {
        val = 0x7ff0000000000000 + (static_cast<uint64_t>(i) << 42);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  負のゼロ。          //
    //  0x8000              //
    std::cout   <<  std::hex    <<  std::setw(16);
    std::cout   <<  "0x8000000000000000,\n";

    //  負の非正規化数。    //
    //  0x8001              //
    std::cout   <<  "0xbe70000000000000,\n";

    //  0x8002 ... 0x8003   //
    std::cout   <<  "0xbe80000000000000,\n";
    std::cout   <<  "0xbe88000000000000,\n";

    //  0x8004 ... 0x8007   //
    for ( int i = 4; i <= 7; ++ i ) {
        val = 0xbe80000000000000 + (static_cast<uint64_t>(i) << 50);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x8008 ... 0x800f   //
    for ( int i = 8; i <= 15; ++ i ) {
        val = 0xbe90000000000000 + (static_cast<uint64_t>(i) << 49);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x8010 ... 0x801f   //
    for ( int i = 16; i <= 31; ++ i ) {
        val = 0xbea0000000000000 + (static_cast<uint64_t>(i) << 48);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x8020 ... 0x803f   //
    for ( int i = 32; i <= 63; ++ i ) {
        val = 0xbeb0000000000000 + (static_cast<uint64_t>(i) << 47);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x8040 ... 0x807f   //
    for ( int i = 64; i <= 127; ++ i ) {
        val = 0xbec0000000000000 + (static_cast<uint64_t>(i) << 46);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x8080 ... 0x80ff   //
    for ( int i = 128; i <= 255; ++ i ) {
        val = 0xbed0000000000000 + (static_cast<uint64_t>(i) << 45);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x8100 ... 0x81ff   //
    for ( int i = 256; i <= 511; ++ i ) {
        val = 0xbee0000000000000 + (static_cast<uint64_t>(i) << 44);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  0x8200 ... 0x83ff   //
    for ( int i = 512; i <= 1023; ++ i ) {
        val = 0xbef0000000000000 + (static_cast<uint64_t>(i) << 43);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  負の正規化数。      //
    //  0x8400 ... 0xfbff   //
    for ( int i = 0; i <= 30719; ++ i ) {
        val = 0xbf10000000000000 + (static_cast<uint64_t>(i) << 42);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }

    //  INF, NaN            //
    //  0xfc00 ... 0xffff   //
    for ( int i = 0; i <= 1022; ++ i ) {
        val = 0xfff0000000000000 + (static_cast<uint64_t>(i) << 42);
        std::cout   <<  "0x"   <<  val  <<  ",\n";
    }
    std::cout   <<  "0xfffffc0000000000\n";

    return ( 0 );
}

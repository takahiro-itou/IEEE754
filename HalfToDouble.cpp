

#if 0
CXX=g++
program=HalfToDouble
${CXX}  -O2 -o bin_${program}.exe  ${program}.cpp;
exit  0
#endif

#include    <stdint.h>
#include    <stdio.h>

constexpr   uint64_t
g_TableHalfToDouble[65536] = {
#    include    "table.csv"
};

template  <typename T>
T
pointer_cast(const void * ptr)
{
    return ( static_cast<T>(ptr) );
}

double  convertHalfToDouble(const uint16_t val)
{
    return  *(pointer_cast<const double *>(&g_TableHalfToDouble[val]));
}

double  convertHalfToDouble(const void * ptr){
    uint16_t tmp = *(static_cast<const uint16_t *>(ptr));
    return  convertHalfToDouble(tmp);
}

int  main(int argc, char * argv[])
{
    uint16_t    testee;
    double      result;

    testee  = 0x3c00;   //  1.0
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x3c01;   //  1.0009765625
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x3c02;   //  1.001953125
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x3fff;   //  1.9990234375
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x4000;   //  2
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0xc000;   //  -2
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x7bfe;   //  65472
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x7bff;   //  65504
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0xfbff;   //  -65504
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x0400;   //  2^{-14}
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x0001;   //  2^{-28}
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x0000;   //  0
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x8000;   //  -0
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x7c00;   //  + INF
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0xfc00;   //  - INF
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    testee  = 0x3555;   //  0.33325...
    result  = convertHalfToDouble(testee);
    printf("0x%04x = %.20f\n", testee, result);

    return ( 0 );
}

